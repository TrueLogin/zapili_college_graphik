class ColleaguesMailer < ActionMailer::Base
  default from: "zapili.college.graphik@herokuapp.com"
  default to: ZapiliCollegeGraphik.author_email

  def feedback(feedback)
    subject = "[ZCG] New feedback from #{feedback.user_name}"
    @feedback = feedback
    mail(subject: subject)
  end
end
