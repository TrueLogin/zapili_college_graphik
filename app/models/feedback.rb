class Feedback < ActiveRecord::Base
  validates :user_name, presence: true
  validates :user_email, presence: true, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :body, presence: true

  def send_to_author!
    ColleaguesMailer.feedback(self).deliver!
  end
end
