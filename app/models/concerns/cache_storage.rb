module CacheStorage
  extend ActiveSupport::Concern

  included do
    CHART_EXPIRATION = 1.hour
    attr_accessor :cache_key
  end

  def store
    @cache_key = generate_cache_key
    return true if Rails.cache.exist?(@cache_key)
    Rails.cache.write(@cache_key, self.to_json, expires_in: CHART_EXPIRATION)
  end

  module ClassMethods
    def fetch_from_cache(cache_key)
      Rails.cache.read(cache_key)
    end
  end

  private
    def generate_cache_key
      Digest::SHA1.hexdigest self.to_json
    end
end
