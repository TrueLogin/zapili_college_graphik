class Chart
  include CacheStorage

  attr_accessor :title, :subtitle, :point_start, :series, :type, :y_axis, :added_at

  alias :id :cache_key
  alias :id= :cache_key=

  DEFAULT_TYPE     = 'spline'
  DEFAULT_SUBTITLE = 'Spline Chart'
  DEFAULT_Y_AXIS   = 'Values'

  DEFAULT_SERIES_NUMBER = 10

  # params{ title:, subtitle:, point_start:, series:, type:, ... }
  def initialize(params)
    @title       = params[:title]
    @subtitle    = params[:subtitle].presence || DEFAULT_SUBTITLE
    @point_start = params[:point_start]
    @series      = params[:series]
    @type        = params[:type].presence     || DEFAULT_TYPE
    @y_axis      = params[:y_axis].presence   || DEFAULT_Y_AXIS
    @cache_key   = generate_cache_key
    @added_at    = params[:added_at].presence || Time.now
  end

  def expires_at
    Time.at(added_at.to_time.to_i + CHART_EXPIRATION)
  end

  def split
    @series.in_groups_of(DEFAULT_SERIES_NUMBER, false).map do |series_group|
      self.class.new({
        title:       @title,
        subtitle:    @subtitle,
        point_start: @point_start,
        series:      series_group,
        type:        @type,
        y_axis:      @y_axis,
        added_at:    self.added_at
      })
    end
  end

  def splittable?
    @series.size > DEFAULT_SERIES_NUMBER
  end

  def to_json
    {
      title:       title,
      subtitle:    subtitle,
      point_start: point_start,
      series:      series,
      type:        type,
      y_axis:      y_axis,
      added_at:    added_at
    }.to_json
  end

  def self.initialize_with_id(id)
    data = JSON.parse(self.fetch_from_cache(id)) rescue {}
    return if data.blank?
    self.new(data.deep_symbolize_keys)
  end

  # params{ title: '', subtitle: '', type: '', file:, ... }
  def self.initialize_from_params(params)
    data = RawDataProcessor.new(params['file']).process
    self.new({
      title:       params['title'],
      subtitle:    params['subtitle'],
      point_start: self.get_point_start(data),
      series:      self.convert_to_series(data),
      type:        params['type'],
      y_axis:      params['y_axis']
    })
  end

  private

    def self.convert_to_series(data)
      data.map{ |name, _data| { name: name, data: _data.values.map(&:to_f) } }
    end

    def self.get_point_start(data)
      data.first[1].keys.first
    end

end
