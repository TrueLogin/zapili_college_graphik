class RawDataProcessor
  def initialize(file)
    @file = file
  end

  def process
    parsed_data = group_parsed_data(parse)
    data_hash = parsed_data_to_hash(parsed_data)
    fill_with_nils(data_hash)
  end

  private

    def parse
      parsed_data = @file.set_encoding('UTF-8').each_with_object([]) do |line, result|
        result << line.gsub(/\s/, '').split(',').map(&:strip)
      end
      @file.close
      parsed_data
    end

    def group_parsed_data(parsed_data)
      parsed_data.split{ |arr| arr.blank? }
    end

    def parsed_data_to_hash(parsed_data)
      parsed_data.each_with_object({}) do |arr, result|
        data_name = arr[0][0]
        arr[0][0] = ""
        arr.each{ |arr_data| arr_data[1] = arr_data[1].to_date.to_s(:iso_8601)}
        data = arr.map{ |d| d.shift; d }
        result[data_name] = data.to_h
      end
    end

    def fill_with_nils(data_hash)
      min_key, max_key = get_min_max_keys(data_hash)
      (min_key.to_date..max_key.to_date).each do |key|
        key = key.to_s(:iso_8601)
        data_hash.each_pair do |_, data|
          next if data[key].present?
          data[key] = nil
        end
      end
      data_hash.each_pair do |name, data|
        data_hash[name] = data.sort_by{ |key, _| key }.to_h
      end
    end

    def get_min_max_keys(data_hash)
      keys = []
      data_hash.each_pair do |_, data|
        keys << data.keys
      end
      keys.flatten.uniq.minmax
    end
end
