class ChartsController < ApplicationController
  def new
  end

  def create
    chart = Chart.initialize_from_params(chart_params)
    chart.store
    redirect_to chart_path(chart.id)
  rescue StandardError => error
    Rails.logger.debug error.message
    Rails.logger.debug error.backtrace[0..3].join("\n\t\t\t")
    flash[:danger] = "Коллега, что-то пошло не так. Попробуйте еще раз"
    redirect_to new_chart_path
  end

  def show
    chart = Chart.initialize_with_id(params[:id])
    if chart.blank?
      flash[:warning] = "Коллега, либо данных нет, либо срок их хранения закончился. Попробуйте снова!"
      redirect_to(new_chart_path) and return
    end
    @charts = chart.splittable? ? chart.split : [chart]
  end

  private
    def chart_params
      params[:chart][:file] = params[:chart][:file].tempfile
      params.require(:chart).permit!
    end
end
