class FeedbacksController < ApplicationController
  def create
    Feedback.create(feedback_params).send_to_author!
    head :ok
  rescue => e
    render json: { message: e.message }, status: :bad_request
  end

  private

    def feedback_params
      params.require(:feedback).permit(:user_name, :user_email, :body)
    end
end
