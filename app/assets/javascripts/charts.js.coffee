verify_required_fields = ->
  is_filled = true
  $("[data-required='true']").each (_, obj) ->
    if $(obj).val().length == 0
      is_filled = false
      parent_group = $(obj).data('parent')
      $(parent_group).addClass('has-error')
  is_filled

$ ->
  $('.submit-form').on 'click', (ev) ->
    ev.preventDefault()
    is_required_fields_filled = verify_required_fields()
    form_to_submit = $(@).data('submit-form')
    $(form_to_submit).submit() if is_required_fields_filled
