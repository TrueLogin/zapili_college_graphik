chart_params = (chart_obj) ->
  title: $(chart_obj).data('title')
  subtitle: $(chart_obj).data('subtitle')
  chart_container: chart_obj
  point_start: $(chart_obj).data('point-start')
  series: $(chart_obj).data('series')
  y_axis: $(chart_obj).data('yaxis')

$ ->
  if $('.chart').length > 0
    $('.chart').each (_, chart) ->
      chart_type = $(chart).data('type')
      new SplineChart(chart_params(chart)).renderChart() if chart_type == 'spline'
      new BarChart(chart_params(chart)).renderChart()    if chart_type == 'bar'
      new AreaChart(chart_params(chart)).renderChart()   if chart_type == 'area'
