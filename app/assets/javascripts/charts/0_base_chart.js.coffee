class @BaseChart
  constructor: (options) -> @options = @_build_options(options)

  _build_options: (options) -> options

  renderChart: -> new Highcharts.Chart(@options)
