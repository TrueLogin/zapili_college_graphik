class @SplineChart extends BaseChart
  constructor: (options) -> super(options)

  _build_options: (options) ->
    title:
      text: options.title
    subtitle:
      text: options.subtitle
    chart:
      type: 'spline'
      renderTo: options.chart_container
      zoomType: 'x'
      resetZoomButton:
        position:
          x: -30
          y: -45
    xAxis:
      type: 'datetime'
    yAxis:
      title:
        text: options.y_axis
    tooltip:
      shared: true
      crosshairs: true
    plotOptions:
      spline:
        lineWidth: 2
        states:
          hover:
            lineWidth: 3
        marker:
          enabled: false
        pointStart: Date.parse(options.point_start)
        pointInterval: 1000 * 3600 * 24 # one day
    series: options.series
