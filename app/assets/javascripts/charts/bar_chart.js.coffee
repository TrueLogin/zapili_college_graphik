class @BarChart extends BaseChart
  constructor: (options) -> super(options)

  _build_options: (options) ->
    title:
      text: options.title
    subtitle:
      text: options.subtitle
    chart:
      type: 'column'
      renderTo: options.chart_container
      zoomType: 'x'
      resetZoomButton:
        position:
          x: -30
          y: -45
    xAxis:
      type: 'datetime'
    yAxis:
      title:
        text: options.y_axis
    tooltip:
      shared: true
    series: @_prepare_series(options)

  # [{
  #   name: 'Name_1',
  #   data: [
  #           [date_1.to_i, value_1],
  #           [date_2.to_i, value_2],
  #         ]
  # }]
  _prepare_series: (options) ->
    point_start = Date.parse(options.point_start)
    point_interval = 1000 * 3600 * 24 # one day

    options.series.map (_series) ->
      name: _series.name
      data: _series.data.map (_data, index) ->
        [point_start + (point_interval * index), _data]
