$ ->
  $('form#new_feedback').on 'ajax:before', () -> $('#request-processing-modal').modal(keyboard: false, backdrop: 'static')

  $('form#new_feedback').on 'ajax:success', () -> $('#request-state-alert').collapse('hide')

  $('form#new_feedback').on 'ajax:error', (_, response) ->
    response = JSON.parse(response.responseText)
    console.log response.message
    $('#request-processing-modal').modal('hide')

  $('#request-state-alert').on 'hidden.bs.collapse', () ->
    $('#success-alert').collapse('show')
    setTimeout(() ->
      window.location.reload()
    , 1500)

  $('#request-processing-modal').on 'hide.bs.modal', () -> $('#error-alert').collapse('show')
