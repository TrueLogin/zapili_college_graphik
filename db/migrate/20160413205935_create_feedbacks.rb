class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :user_name
      t.string :user_email
      t.text :body

      t.timestamps
    end
  end
end
