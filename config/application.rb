require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

APP_CONFIG = YAML.load_file(File.expand_path(File.join('config', 'application_config.yml')))[Rails.env]

module ZapiliCollegeGraphik
  class Application < Rails::Application
    config.serve_static_assets = true
  end

  def self.new_issue_endpoint
    @new_issue_endpoint ||= APP_CONFIG.fetch('new_issue_endpoint', '')
  end

  def self.fork_endpoint
    @fork_endpoint ||= APP_CONFIG.fetch('fork_endpoint', '')
  end

  def self.author_email
    @author_email ||= APP_CONFIG.fetch('author_email', '')
  end

  def self.colleague_profile_endpoint
    @colleague_profile_endpoint ||= APP_CONFIG.fetch('colleague_profile_endpoint', '')
  end

  def self.opu_endpoint
    @opu_endpoint ||= APP_CONFIG.fetch('opu_endpoint', '')
  end

  def self.version
    '0.0.5'
  end
end
