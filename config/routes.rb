Rails.application.routes.draw do
  root to: 'home#index'
  resources :about, only: %i(index)
  resources :charts, only: %i(new create show)
  resources :feedbacks, only: %i(create)
end
